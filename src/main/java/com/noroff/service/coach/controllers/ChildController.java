package com.noroff.service.coach.controllers;

import com.noroff.service.coach.data_access.ChildRepository;
import com.noroff.service.coach.models.Child;
import com.noroff.service.coach.models.Team;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/child")
public class ChildController {

    ChildRepository childRepository = new ChildRepository();

    @GetMapping("/parents/{username}")
    public ResponseEntity<ArrayList<Child>> getParentsOfChild(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Child> parents = childRepository.getParentsOfChild(username);

        if(parents == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(parents, status);
    }

    @PostMapping("/addChildWithTwoParents")
    public ResponseEntity<Child> addChildWithTwoParents(@RequestBody Child child){
        HttpStatus status = childRepository.addChildWithTwoParents(child);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(child, status);
    }

    @PostMapping("/addChildWithOneParent")
    public ResponseEntity<Child> addChildWithOneParent(@RequestBody Child child){
        HttpStatus status = childRepository.addChildWithOneParent(child);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(child, status);
    }

    @PutMapping("/{username}")
    public ResponseEntity<Child> updateChildParentRelationship(@PathVariable String username, @RequestBody Child child) {
        HttpStatus status = childRepository.updateChildParentRelationship(username, child);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(child, status);
    }
}
