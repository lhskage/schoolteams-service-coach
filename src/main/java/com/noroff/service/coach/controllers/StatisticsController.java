package com.noroff.service.coach.controllers;

import com.noroff.service.coach.data_access.StatisticsRepository;
import com.noroff.service.coach.models.statistics.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/statistic")
public class StatisticsController {

    StatisticsRepository statisticsRepository = new StatisticsRepository();

    // Get statistics for specified team
    @GetMapping("/played/team/{team_id}")
    public ResponseEntity<MatchesPlayed> getNumberOfMatchesByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        MatchesPlayed matches = statisticsRepository.getNumberOfPlayedMatchesByTeam(team_id);

        if(matches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matches, status);
    }

    @GetMapping("/wins/team/{team_id}")
    public ResponseEntity<MatchesWon> getNumberOfWinsByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        MatchesWon stats = statisticsRepository.getNumberOfWinsByTeam(team_id);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    @GetMapping("/losses/team/{team_id}")
    public ResponseEntity<MatchesLost> getNumberOfLossesByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        MatchesLost stats = statisticsRepository.getNumberOfLossesByTeam(team_id);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    @GetMapping("/draws/team/{team_id}")
    public ResponseEntity<MatchesDrawn> getNumberOfDrawsByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        MatchesDrawn stats = statisticsRepository.getNumberOfDrawsByTeam(team_id);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    @GetMapping("/stats/team/{team_id}")
    public ResponseEntity<Statistics> getStatsByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        Statistics stats = statisticsRepository.getStatsByTeam(team_id);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    // Get statistics for specified school
    @GetMapping("/played/school/{school_id}")
    public ResponseEntity<MatchesPlayed> getNumberOfMatchesBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        MatchesPlayed playedMatches = statisticsRepository.getNumberOfPlayedMatchesBySchool(school_id);

        if(playedMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(playedMatches, status);
    }

    @GetMapping("/wins/school/{school_id}")
    public ResponseEntity<MatchesWon> getNumberOfWinsBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        MatchesWon stats = statisticsRepository.getNumberOfWinsBySchool(school_id);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    @GetMapping("/losses/school/{school_id}")
    public ResponseEntity<MatchesLost> getNumberOfLossesBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        MatchesLost stats = statisticsRepository.getNumberOfLossesBySchool(school_id);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    @GetMapping("/draws/school/{school_id}")
    public ResponseEntity<MatchesDrawn> getNumberOfDrawsBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        MatchesDrawn stats = statisticsRepository.getNumberOfDrawsBySchool(school_id);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    @GetMapping("/stats/school/{school_id}")
    public ResponseEntity<Statistics> getStatsBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        Statistics stats = statisticsRepository.getStatsBySchool(school_id);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    // Get statistics for specified player
    @GetMapping("/played/player/{username}")
    public ResponseEntity<MatchesPlayed> getNumberOfMatchesByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        MatchesPlayed playedMatches = statisticsRepository.getNumberOfPlayedMatchesByPlayer(username);

        if(playedMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(playedMatches, status);
    }

    @GetMapping("/wins/player/{username}")
    public ResponseEntity<MatchesWon> getNumberOfWinsByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        MatchesWon matchesWon = statisticsRepository.getNumberOfWinsByPlayer(username);

        if(matchesWon == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matchesWon, status);
    }

    @GetMapping("/draws/player/{username}")
    public ResponseEntity<MatchesDrawn> getNumberOfDrawsByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        MatchesDrawn matchesDrawn = statisticsRepository.getNumberOfDrawsByPlayer(username);

        if(matchesDrawn == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matchesDrawn, status);
    }

    @GetMapping("/loss/player/{username}")
    public ResponseEntity<MatchesLost> getNumberOfLossesByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        MatchesLost matchesLost = statisticsRepository.getNumberOfLossesByPlayer(username);

        if(matchesLost == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matchesLost, status);
    }

    @GetMapping("/stats/player/{username}")
    public ResponseEntity<PlayerStatistic> getStatsByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        PlayerStatistic stats = statisticsRepository.getStatsByPlayer(username);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

}