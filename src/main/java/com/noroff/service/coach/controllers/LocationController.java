package com.noroff.service.coach.controllers;

import com.noroff.service.coach.data_access.LocationRepository;
import com.noroff.service.coach.models.Location;
import com.noroff.service.coach.models.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/location")
public class LocationController {

    LocationRepository locationRepository = new LocationRepository();

    @GetMapping()
    public ResponseEntity<ArrayList<Location>> getAllLocations(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Location> locations = locationRepository.getAllLocations();

        if(locations == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(locations, status);
    }

    @PostMapping("/add")
    public ResponseEntity<Location> addOptional(@RequestBody Location location) {
        Pair<HttpStatus, Integer> res = locationRepository.addLocation(location);

        if (res.getU() != -1) {
            location.setId(res.getU());
        }

        if (res.getT() != HttpStatus.CREATED) {
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(location, res.getT());
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Location> updateOptional(@PathVariable int id, @RequestBody Location location){
        Pair<HttpStatus, Integer> status = locationRepository.updateLocation(id, location);

        if(status.getU() != -1){
            location.setId(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(location, status.getT());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Location> deleteLocation(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation of delete mapping

        return new ResponseEntity<>(null, status);
    }
}
