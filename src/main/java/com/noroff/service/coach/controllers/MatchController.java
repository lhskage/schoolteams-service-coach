package com.noroff.service.coach.controllers;

import com.noroff.service.coach.data_access.MatchRepository;
import com.noroff.service.coach.models.Match;
import com.noroff.service.coach.models.MatchDetailed;
import com.noroff.service.coach.models.MatchResult;
import com.noroff.service.coach.models.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/matches")
public class MatchController {

    MatchRepository matchRepository = new MatchRepository();

    @PostMapping("/create")
    public ResponseEntity<Match> addMatch(@RequestBody Match match){
        Pair<HttpStatus, Integer> status = matchRepository.addMatch(match);

        if(status.getU() != -1){
            match.setId(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(match, status.getT());
    }

    @PostMapping("/create/match_result")
    public ResponseEntity<MatchResult> addMatchResult(@RequestBody MatchResult match){
        Pair<HttpStatus, Integer> status = matchRepository.addMatchResult(match);

        if(status.getU() != -1){
            match.setMatch_id(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(match, status.getT());
    }

    @PutMapping("/update/{match_id}")
    public ResponseEntity<Match> updateMatch(@PathVariable int match_id, @RequestBody Match match){
        Pair<HttpStatus, Integer> status = matchRepository.updateMatch(match_id, match);

        if(status.getU() != -1){
            match.setId(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(match, status.getT());
    }

    @PutMapping("/update/result/{match_id}")
    public ResponseEntity<MatchResult> updateMatchResult(@PathVariable int match_id, @RequestBody MatchResult matchResult){
        Pair<HttpStatus, Integer> status = matchRepository.updateMatchResult(match_id, matchResult);

        if(status.getU() != -1){
            matchResult.setMatch_id(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(matchResult, status.getT());
    }

    @PutMapping("/cancel/{match_id}")
    public ResponseEntity<Match> cancelMatch(@PathVariable int match_id, @RequestBody Match match){
        Pair<HttpStatus, Integer> status = matchRepository.cancelMatch(match_id, match);

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(match, status.getT());
    }

    @GetMapping()
    public ResponseEntity<ArrayList<MatchDetailed>> getAllMatches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matchDetaileds = matchRepository.getAllMatches();

        if(matchDetaileds == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matchDetaileds, status);
    }

    @GetMapping("/{username}")
    public ResponseEntity<ArrayList<MatchDetailed>> getMatchesWhereIncluded(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> cancelledMatches = matchRepository.getMatchesWhereChildIncluded(username);

        if(cancelledMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(cancelledMatches, status);
    }

    @GetMapping("/upcoming")
    public ResponseEntity<ArrayList<MatchDetailed>> getUpcomingMatches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matchDetaileds = matchRepository.getUpcomingMatches();

        if(matchDetaileds == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matchDetaileds, status);
    }

    @GetMapping("/upcoming/{username}")
    public ResponseEntity<ArrayList<MatchDetailed>> getUpcomingMatchesByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matchDetaileds = matchRepository.getUpcomingMatchesByPlayer(username);

        if(matchDetaileds == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matchDetaileds, status);
    }

    @GetMapping("/previous")
    public ResponseEntity<ArrayList<MatchDetailed>> getPreviousMatches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> cancelledMatches = matchRepository.getPreviousMatches();

        if(cancelledMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(cancelledMatches, status);
    }

    @GetMapping("/previous/{username}")
    public ResponseEntity<ArrayList<MatchDetailed>> getPreviousMatchesByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matchDetaileds = matchRepository.getPreviousMatchesByPlayer(username);

        if(matchDetaileds == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matchDetaileds, status);
    }

    @GetMapping("/cancelled")
    public ResponseEntity<ArrayList<MatchDetailed>> getCancelledMatches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> cancelledMatches = matchRepository.getCancelledMatches();

        if(cancelledMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(cancelledMatches, status);
    }

    @GetMapping("/school/{school_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getMatchesBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matches = matchRepository.getMatchesBySchool(school_id);

        if(matches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matches, status);
    }

    @GetMapping("/upcoming/school/{school_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getUpcomingMatchesBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> upcomingMatches = matchRepository.getUpcomingMatchesBySchool(school_id);

        if(upcomingMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(upcomingMatches, status);
    }

    @GetMapping("/previous/school/{school_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getPreviousMatchesBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> previousMatches = matchRepository.getPreviousMatchesBySchool(school_id);

        if(previousMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(previousMatches, status);
    }

    @GetMapping("/cancelled/school/{school_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getCancelledMatchesBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> cancelledMatches = matchRepository.getCancelledMatchesBySchool(school_id);

        if(cancelledMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(cancelledMatches, status);
    }

    @GetMapping("/team/{team_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getMatchesByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matches = matchRepository.getMatchesByTeam(team_id);

        if(matches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matches, status);
    }

    @GetMapping("/upcoming/team/{team_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getUpcomingMatchesByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> upcomingMatches = matchRepository.getUpcomingMatchesByTeam(team_id);

        if(upcomingMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(upcomingMatches, status);
    }

    @GetMapping("/previous/team/{team_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getPreviousMatchesByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> previousMatches = matchRepository.getPreviousMatchesByTeam(team_id);

        if(previousMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(previousMatches, status);
    }

    @GetMapping("/cancelled/team/{team_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getCancelledMatchesByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> cancelledMatches = matchRepository.getCancelledMatchesByTeam(team_id);

        if(cancelledMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(cancelledMatches, status);
    }

    @GetMapping("/unregistered_result/coach/{coach}")
    public ResponseEntity<ArrayList<MatchDetailed>> getUnregisteredPreviousMatchResultsByCoach(@PathVariable String coach){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> unregisteredMatchResults = matchRepository.getUnregisteredPreviousMatchResultsByCoach(coach);

        if(unregisteredMatchResults == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(unregisteredMatchResults, status);
    }
}