package com.noroff.service.coach.controllers;

import com.noroff.service.coach.data_access.SportRepository;
import com.noroff.service.coach.models.Pair;
import com.noroff.service.coach.models.School;
import com.noroff.service.coach.models.Sport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/sport")
public class SportController {
    SportRepository sportRepository = new SportRepository();

    @GetMapping()
    public ResponseEntity<ArrayList<Sport>> getAllSports(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Sport> sports = sportRepository.getAllSports();

        if(sports == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(sports, status);
    }

    @PostMapping("/add")
    public ResponseEntity<Sport> addSport(@RequestBody Sport sport) {
        Pair<HttpStatus, Integer> res = sportRepository.addSport(sport);

        if (res.getU() != -1) {
            sport.setId(res.getU());
        }

        if (res.getT() != HttpStatus.CREATED) {
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(sport, res.getT());
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Sport> updateSport(@PathVariable int id, @RequestBody Sport sport){
        Pair<HttpStatus, Integer> status = sportRepository.updateSport(id, sport);

        if(status.getU() != -1){
            sport.setId(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(sport, status.getT());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<School> deleteSport(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation of delete mapping

        return new ResponseEntity<>(null, status);
    }
}
