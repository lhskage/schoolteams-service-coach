package com.noroff.service.coach.controllers;

import com.noroff.service.coach.data_access.SchoolRepository;
import com.noroff.service.coach.models.Pair;
import com.noroff.service.coach.models.School;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/school")
public class SchoolController {
    SchoolRepository schoolRepository = new SchoolRepository();

    @GetMapping()
    public ResponseEntity<ArrayList<School>> getAllSchools(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<School> schools = schoolRepository.getAllSchools();

        if(schools == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(schools, status);
    }

    @GetMapping("/player/{username}")
    public ResponseEntity<School> getSchoolByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        School stats = schoolRepository.getSchoolByPlayer(username);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    @GetMapping("/user/{username}")
    public ResponseEntity<School> getSchoolByUsername(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        School stats = schoolRepository.getSchoolByUsername(username);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    @PostMapping("/add")
    public ResponseEntity<School> addSchool(@RequestBody School school) {
        Pair<HttpStatus, Integer> res = schoolRepository.addSchool(school);

        if (res.getU() != -1) {
            school.setId(res.getU());
        }

        if (res.getT() != HttpStatus.CREATED) {
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(school, res.getT());
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<School> updateSchool(@PathVariable int id, @RequestBody School school){
        Pair<HttpStatus, Integer> status = schoolRepository.updateSchool(id, school);

        if(status.getU() != -1){
            school.setId(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(school, status.getT());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<School> deleteSchool(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation of delete mapping

        return new ResponseEntity<>(null, status);
    }
}
