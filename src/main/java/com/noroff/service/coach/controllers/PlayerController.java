package com.noroff.service.coach.controllers;

import com.noroff.service.coach.data_access.PlayerRepository;
import com.noroff.service.coach.models.Player;
import com.noroff.service.coach.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/player")
public class PlayerController {

    PlayerRepository playerRepository = new PlayerRepository();

    @PostMapping("/add")
    public ResponseEntity<Player> addPlayer(@RequestBody Player player){
        HttpStatus status = playerRepository.addPlayer(player);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(player, status);
    }

    @PutMapping("/update/team/{team_id}/player/{username}")
    public ResponseEntity<Player> updatePlayer(@PathVariable int team_id, @PathVariable String username, @RequestBody Player player) {
        HttpStatus status = playerRepository.updatePlayerNumber(team_id, username, player);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(player, status);
    }

    @GetMapping("/players")
    public ResponseEntity<ArrayList<Player>> getAllPlayers(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Player> person = playerRepository.getAllPlayers();

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @GetMapping("/team/{team_id}")
    public ResponseEntity<ArrayList<Player>> getPlayersByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Player> person = playerRepository.getPlayersByTeam(team_id);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @GetMapping("/notPartOfTeam/{team_id}")
    public ResponseEntity<ArrayList<User>> getPlayersNotInTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = playerRepository.getPlayersNotInTeam(team_id);

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @GetMapping("/school/{school_id}")
    public ResponseEntity<ArrayList<Player>> getPlayersBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Player> person = playerRepository.getPlayersBySchool(school_id);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }
}
