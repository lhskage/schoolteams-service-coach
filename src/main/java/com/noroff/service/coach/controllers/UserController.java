package com.noroff.service.coach.controllers;

import com.noroff.service.coach.data_access.UserRepository;
import com.noroff.service.coach.models.Toggle2FA;
import com.noroff.service.coach.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/user")
public class UserController {
    UserRepository userRepository = new UserRepository();

    @GetMapping()
    public ResponseEntity<ArrayList<User>> getAllUsers(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllUsers();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @GetMapping("/admin")
    public ResponseEntity<ArrayList<User>> getAllAdministrators(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllAdministrators();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @GetMapping("/coach")
    public ResponseEntity<ArrayList<User>> getAllCoaches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllCoaches();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @GetMapping("/parents")
    public ResponseEntity<ArrayList<User>> getAllParents(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllParents();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @GetMapping("/players")
    public ResponseEntity<ArrayList<User>> getAllPlayers(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllPlayers();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @PostMapping("/add")
    public ResponseEntity<User> addUser(@RequestBody User user){
        HttpStatus status = userRepository.addUser(user);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(user, status);
    }

    @PutMapping("/update/{username}")
    public ResponseEntity<User> updateUser(@PathVariable String username, @RequestBody User user) {
        HttpStatus status = userRepository.updateUser(username, user);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(user, status);
    }

    @GetMapping("/2fa/active/{username}")
    public ResponseEntity<Boolean> get2faActive(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Boolean active = userRepository.getActive(username);

        if(active == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(active, status);
    }

    @PatchMapping("/2fa")
    public ResponseEntity<Boolean> toggleActive2fa(@RequestBody Toggle2FA toggle2FA){
        HttpStatus status = HttpStatus.OK;
        Boolean active = userRepository.update2FA(toggle2FA);

        if(active == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(active, status);
    }

    @GetMapping("/2fa/qr/{username}")
    public ResponseEntity<String> get2faQrUrl(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        String url = userRepository.getQrUrl(username);

        if(url == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(url, status);
    }
}