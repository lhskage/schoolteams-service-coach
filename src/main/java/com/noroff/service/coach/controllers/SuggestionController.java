package com.noroff.service.coach.controllers;

import com.noroff.service.coach.data_access.SuggestionRepository;
import com.noroff.service.coach.models.Suggestion;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/suggestion")
class SuggestionController {
    SuggestionRepository suggestionRepository = new SuggestionRepository();

    @GetMapping("/{username}")
    public ResponseEntity<ArrayList<Suggestion>> getSuggestionsByRequester(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Suggestion> messages = suggestionRepository.getSuggestionsByRequester(username);

        if(messages == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    @PostMapping()
    public ResponseEntity<Suggestion> createSuggestion(@RequestBody Suggestion suggestion){
        HttpStatus status = suggestionRepository.createSuggestion(suggestion);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(suggestion, status);
    }
}
