package com.noroff.service.coach.controllers;

import com.noroff.service.coach.data_access.TeamRepository;
import com.noroff.service.coach.models.Pair;
import com.noroff.service.coach.models.Team;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/team")
public class TeamController {

    TeamRepository teamRepository = new TeamRepository();

    @GetMapping()
    public ResponseEntity<ArrayList<Team>> getAllTeams(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> teams = teamRepository.getAllTeams();

        if(teams == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(teams, status);
    }

    @GetMapping("/{team_id}")
    public ResponseEntity<Team> getTeamById(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        Team person = teamRepository.getTeamById(team_id);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @GetMapping("/school/{school_id}")
    public ResponseEntity<ArrayList<Team>> getTeamBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> person = teamRepository.getTeamsBySchool(school_id);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @GetMapping("/sport/{sport_id}")
    public ResponseEntity<ArrayList<Team>> getTeamBySport(@PathVariable int sport_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> person = teamRepository.getTeamBySport(sport_id);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @GetMapping("/coach/{coach}")
    public ResponseEntity<ArrayList<Team>> getTeamByCoach(@PathVariable String coach){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> person = teamRepository.getTeamByCoach(coach);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @GetMapping("/player/{player}")
    public ResponseEntity<ArrayList<Team>> getTeamByPlayer(@PathVariable String player){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> person = teamRepository.getTeamByPlayer(player);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @PostMapping("/add")
    public ResponseEntity<Team> addTeam(@RequestBody Team team) {
        Pair<HttpStatus, Integer> res = teamRepository.addTeam(team);

        if (res.getU() != -1) {
            team.setId(res.getU());
        }

        if (res.getT() != HttpStatus.CREATED) {
            return new ResponseEntity<>(null, res.getT());
        }
        return new ResponseEntity<>(team, res.getT());
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Team> updateOptional(@PathVariable int id, @RequestBody Team team){
        Pair<HttpStatus, Integer> status = teamRepository.updateTeam(id, team);

        if(status.getU() != -1){
            team.setId(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(team, status.getT());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Team> deleteSchool(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation of delete mapping

        return new ResponseEntity<>(null, status);
    }
}
