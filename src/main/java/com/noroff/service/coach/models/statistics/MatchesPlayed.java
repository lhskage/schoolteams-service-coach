package com.noroff.service.coach.models.statistics;

public class MatchesPlayed {
    private int id;
    private int matches_played;

    public MatchesPlayed(int id, int matches_played) {
        this.id = id;
        this.matches_played = matches_played;
    }

    public MatchesPlayed() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatches_played() {
        return matches_played;
    }

    public void setMatches_played(int matches_played) {
        this.matches_played = matches_played;
    }
}
