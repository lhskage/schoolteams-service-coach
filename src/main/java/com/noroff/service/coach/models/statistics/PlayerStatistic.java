package com.noroff.service.coach.models.statistics;

public class PlayerStatistic {
    private String username;
    private int matches_played;
    private int wins;
    private int draws;
    private int losses;
    private int points;

    public PlayerStatistic(String username, int matches_played, int wins, int draws, int losses, int points) {
        this.username = username;
        this.matches_played = matches_played;
        this.wins = wins;
        this.draws = draws;
        this.losses = losses;
        this.points = points;
    }

    public PlayerStatistic() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getMatches_played() {
        return matches_played;
    }

    public void setMatches_played(int matches_played) {
        this.matches_played = matches_played;
    }
}
