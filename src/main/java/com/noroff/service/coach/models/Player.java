package com.noroff.service.coach.models;

public class Player {
    private String user_username;
    private int team_id;
    private String player_number;

    // Constructor
    public Player(String user_username, int team_id, String player_number) {
        this.user_username = user_username;
        this.team_id = team_id;
        this.player_number = player_number;
    }

    public Player() {
    }

    // Getters and setters
    public String getUsername() {
        return user_username;
    }

    public void setUsername(String user_username) {
        this.user_username = user_username;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public String getPlayer_number() {
        return player_number;
    }

    public void setPlayer_number(String player_number) {
        this.player_number = player_number;
    }
}
