package com.noroff.service.coach.models.statistics;

public class MatchesWon {
    private int id;
    private int matches_won;

    public MatchesWon(int id, int matches_won) {
        this.id = id;
        this.matches_won = matches_won;
    }

    public MatchesWon() {
    }

    public MatchesWon(int wins) {
        this.matches_won = wins;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatches_won() {
        return matches_won;
    }

    public void setMatches_won(int matches_won) {
        this.matches_won = matches_won;
    }
}
