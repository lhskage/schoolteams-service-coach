package com.noroff.service.coach.models;

public class User {
    private String username;
    private String password;
    private int person_id;
    private int role_id;

    // Constructors
    public User(String username, String password, int person_id, int role_id) {
        this.username = username;
        this.password = password;
        this.person_id = person_id;
        this.role_id = role_id;
    }

    public User(String username, int person_id, int role_id) {
        this.username = username;
        this.person_id = person_id;
        this.role_id = role_id;
    }

    public User() {
    }

    // Getters and setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPerson_id() {
        return person_id;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }
}
