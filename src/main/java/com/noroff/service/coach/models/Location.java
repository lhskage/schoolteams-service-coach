package com.noroff.service.coach.models;

public class Location {
    private int id;
    private String street;
    private String number;
    private String postal_code;
    private String name;

    // Constructors
    public Location(int id, String street, String number, String postal_code, String name) {
        this.id = id;
        this.street = street;
        this.number = number;
        this.postal_code = postal_code;
        this.name = name;
    }

    public Location(String street, String number, String postal_code, String name) {
        this.street = street;
        this.number = number;
        this.postal_code = postal_code;
        this.name = name;
    }

    public Location() {
    }

    // Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
