package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Pair;
import com.noroff.service.coach.models.Person;
import org.springframework.http.HttpStatus;

import java.sql.*;

public class PersonRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    public Person getPersonByUsername(String username) {
        Person person = null;

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM person " +
                    "JOIN users " +
                    "ON person.id = users.person_id " +
                    "WHERE users.username = ?;");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){person = new Person(
                    result.getInt("id"),
                    result.getString("firstname"),
                    result.getString("lastname"),
                    result.getString("email"),
                    result.getString("gender"),
                    result.getInt("optional_id")
            );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return person;
    }

    public Pair<HttpStatus, Integer> addPerson(Person person){

        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO person(firstname, lastname, email, gender, optional_id) " +
                            "VALUES (?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, person.getFirstname());
            prep.setString(2, person.getLastname());
            prep.setString(3, person.getEmail());
            prep.setString(4, person.getGender());
            prep.setInt(5, person.getOptional_id());

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next()) {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    public Pair<HttpStatus, Integer> updatePerson(int id, Person person){
        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE person " +
                            "SET (firstname, lastname, email, gender) = (?, ?, ?, ?) " +
                            "WHERE id = ?", Statement.RETURN_GENERATED_KEYS);

            prep.setString(1, person.getFirstname());
            prep.setString(2, person.getLastname());
            prep.setString(3, person.getEmail());
            prep.setString(4, person.getGender());
            prep.setInt(5, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next()) {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }
}
