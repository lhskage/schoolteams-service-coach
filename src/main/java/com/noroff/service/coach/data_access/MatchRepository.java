package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Match;
import com.noroff.service.coach.models.MatchDetailed;
import com.noroff.service.coach.models.MatchResult;
import com.noroff.service.coach.models.Pair;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class MatchRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    // Create match
    public Pair<HttpStatus, Integer> addMatch(Match match) {
        HttpStatus status;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO match(start_time, location_id, team_1_id, team_2_id) " +
                            "VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            prep.setObject(1, match.getStart_time());
            prep.setInt(2, match.getLocation_id());
            prep.setInt(3, match.getTeam_1_id());
            prep.setInt(4, match.getTeam_2_id());

            int result = prep.executeUpdate();
            if(result != 0){
                status = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(status, last_inserted_id);
                }
            }else{
                status = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(status, -1);
    }

    public Pair<HttpStatus, Integer> addMatchResult(MatchResult match) {
        HttpStatus status;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO match_result(home_team_goals, away_team_goals, winner, match_id) " +
                            "VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            prep.setObject(1, match.getHome_team_goals());
            prep.setInt(2, match.getAway_team_goals());
            prep.setString(3, match.getWinner());
            prep.setInt(4, match.getMatch_id());

            int result = prep.executeUpdate();
            if(result != 0){
                status = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt("match_id");
                    return new Pair<>(status, last_inserted_id);
                }
            }else{
                status = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(status, -1);
    }

    // Update match
    public Pair<HttpStatus, Integer> updateMatch(int match_id, Match match) {
        HttpStatus status;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE match SET start_time = ?, cancelled = ?, comment = ?, location_id = ?, " +
                            "team_1_id = ?, team_2_id = ? WHERE id = ?", Statement.RETURN_GENERATED_KEYS);
            prep.setObject(1, match.getStart_time());
            prep.setBoolean(2, match.isCancelled());
            prep.setString(3, match.getComment());
            prep.setInt(4, match.getLocation_id());
            prep.setInt(5, match.getTeam_1_id());
            prep.setInt(6, match.getTeam_2_id());
            prep.setInt(7, match_id);

            int result = prep.executeUpdate();
            if(result != 0){
                status = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(status, last_inserted_id);
                }
            }else{
                status = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(status, -1);
    }

    public Pair<HttpStatus, Integer> cancelMatch(int match_id, Match match) {
        HttpStatus status;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE match SET cancelled = true, comment = ? WHERE id = ?", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, match.getComment());
            prep.setInt(2, match_id);

            int result = prep.executeUpdate();
            if(result != 0){
                status = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(status, last_inserted_id);
                }
            }else{
                status = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(status, -1);
    }

    public Pair<HttpStatus, Integer> updateMatchResult(int match_id, MatchResult matchResult) {
        HttpStatus status;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE match_result SET home_team_goals = ?, away_team_goals = ?, winner = ? " +
                            " WHERE match_id = ?", Statement.RETURN_GENERATED_KEYS);

            prep.setInt(1, matchResult.getHome_team_goals());
            prep.setInt(2, matchResult.getAway_team_goals());
            prep.setString(3, calculateWinner(matchResult));
            prep.setInt(4, match_id);

            int result = prep.executeUpdate();
            if(result != 0){
                status = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next()) {
                    last_inserted_id = rs.getInt("match_id");
                    return new Pair<>(status, last_inserted_id);
                }
            }else{
                status = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(status, -1);
    }

    // Get matches
    public ArrayList<MatchDetailed> getAllMatches() {
        ArrayList<MatchDetailed> matches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner," +
                            "start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id");
            ResultSet result = prep.executeQuery();

            while(result.next()){matches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matches;
    }

    public ArrayList<MatchDetailed> getUpcomingMatches() {
        ArrayList<MatchDetailed> upcomingMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner," +
                            "start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "WHERE match.start_time >= NOW() " +
                            "AND match.cancelled = false");
            ResultSet result = prep.executeQuery();

            while(result.next()){upcomingMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return upcomingMatches;
    }

    public ArrayList<MatchDetailed> getPreviousMatches() {
        ArrayList<MatchDetailed> cancelledMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "WHERE match.start_time <= NOW() " +
                            "AND match.cancelled = false");
            ResultSet result = prep.executeQuery();

            while(result.next()){cancelledMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return cancelledMatches;
    }

    public ArrayList<MatchDetailed> getCancelledMatches() {
        ArrayList<MatchDetailed> cancelledMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "WHERE cancelled = TRUE");
            ResultSet result = prep.executeQuery();

            while(result.next()){cancelledMatches.add(new MatchDetailed(
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return cancelledMatches;
    }

    // Get matches by player (username)
    public ArrayList<MatchDetailed> getUpcomingMatchesByPlayer(String username) {
        ArrayList<MatchDetailed> upcomingMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT DISTINCT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN player AS home " +
                            "on match.team_1_id = home.team_id " +
                            "JOIN player AS away " +
                            "on match.team_2_id = away.team_id " +
                            "WHERE (home.user_username = ? " +
                            "OR away.user_username = ?) AND " +
                            "match.start_time >= NOW() " +
                            "AND match.cancelled = false");
            prep.setString(1, username);
            prep.setString(2, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){upcomingMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return upcomingMatches;
    }

    public ArrayList<MatchDetailed> getPreviousMatchesByPlayer(String username) {
        ArrayList<MatchDetailed> previousMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT DISTINCT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN player AS home " +
                            "on match.team_1_id = home.team_id " +
                            "JOIN player AS away " +
                            "on match.team_2_id = away.team_id " +
                            "WHERE (home.user_username = ? " +
                            "OR away.user_username = ?) AND " +
                            "match.start_time <= NOW() " +
                            "AND match.cancelled = false");
            prep.setString(1, username);
            prep.setString(2, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){previousMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return previousMatches;
    }

    public ArrayList<MatchDetailed> getMatchesWhereChildIncluded(String username) {

        ArrayList<MatchDetailed> includedMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT DISTINCT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN player AS home " +
                            "on match.team_1_id = home.team_id " +
                            "JOIN player AS away " +
                            "on match.team_2_id = away.team_id " +
                            "WHERE home.user_username = ? " +
                            "OR away.user_username = ?;");
            prep.setString(1, username);
            prep.setString(2, username);

            ResultSet result = prep.executeQuery();

            while(result.next()){includedMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return includedMatches;
    }

    // Get matches by school (school_id)
    public ArrayList<MatchDetailed> getMatchesBySchool(int school_id) {
        ArrayList<MatchDetailed> matches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school " +
                            "ON home_team.school_id = home_team_school.id " +
                            "JOIN school AS away_team_school " +
                            "ON away_team.school_id = away_team_school.id " +
                            "WHERE home_team_school.id = ?" +
                            "OR away_team_school.id = ?");

            prep.setInt(1, school_id);
            prep.setInt(2, school_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){matches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matches;
    }

    public ArrayList<MatchDetailed> getUpcomingMatchesBySchool(int school_id) {
        ArrayList<MatchDetailed> upcomingMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school " +
                            "ON home_team.school_id = home_team_school.id " +
                            "JOIN school AS away_team_school " +
                            "ON away_team.school_id = away_team_school.id " +
                            "WHERE (home_team_school.id = ?" +
                            "OR away_team_school.id = ?) AND " +
                            "match.start_time >= NOW() AND match.cancelled = false");

            prep.setInt(1, school_id);
            prep.setInt(2, school_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){upcomingMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return upcomingMatches;
    }

    public ArrayList<MatchDetailed> getPreviousMatchesBySchool(int school_id) {
        ArrayList<MatchDetailed> previousMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school " +
                            "ON home_team.school_id = home_team_school.id " +
                            "JOIN school AS away_team_school " +
                            "ON away_team.school_id = away_team_school.id " +
                            "WHERE (home_team_school.id = ?" +
                            "OR away_team_school.id = ?) AND " +
                            "match.start_time <= NOW() AND match.cancelled = false");
            prep.setInt(1, school_id);
            prep.setInt(2, school_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){previousMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return previousMatches;
    }

    public ArrayList<MatchDetailed> getCancelledMatchesBySchool(int school_id) {
        ArrayList<MatchDetailed> cancelledMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school " +
                            "ON home_team.school_id = home_team_school.id " +
                            "JOIN school AS away_team_school " +
                            "ON away_team.school_id = away_team_school.id " +
                            "WHERE (home_team_school.id = ?" +
                            "OR away_team_school.id = ?) AND match.cancelled = true");
            prep.setInt(1, school_id);
            prep.setInt(2, school_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){cancelledMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return cancelledMatches;
    }

    // Get matches by team (team_id)
    public ArrayList<MatchDetailed> getMatchesByTeam(int team_id) {
        ArrayList<MatchDetailed> matches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id  " +
                            "WHERE (home_team.id = ? " +
                            "OR away_team.id = ?)");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){matches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matches;
    }

    public ArrayList<MatchDetailed> getUpcomingMatchesByTeam(int team_id) {
        ArrayList<MatchDetailed> upcomingMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id  " +
                            "WHERE (home_team.id = ? " +
                            "OR away_team.id = ?) AND " +
                            "match.start_time >= NOW() AND match.cancelled = false");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){upcomingMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return upcomingMatches;
    }

    public ArrayList<MatchDetailed> getPreviousMatchesByTeam(int team_id) {
        ArrayList<MatchDetailed> previousMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id  " +
                            "WHERE (home_team.id = ? " +
                            "OR away_team.id = ?) AND " +
                            "match.start_time <= NOW() AND match.cancelled = false");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){previousMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return previousMatches;
    }

    public ArrayList<MatchDetailed> getCancelledMatchesByTeam(int team_id) {
        ArrayList<MatchDetailed> cancelledMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id  " +
                            "WHERE (home_team.id = ? " +
                            "OR away_team.id = ?) AND " +
                            "match.cancelled = true");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){cancelledMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return cancelledMatches;
    }

    private String calculateWinner(MatchResult matchResult) {
        String winner;
        if(matchResult.getHome_team_goals() > matchResult.getAway_team_goals()) {
            winner = "H";
        } else if(matchResult.getHome_team_goals() < matchResult.getAway_team_goals()) {
            winner = "A";
        } else {
            winner = "D";
        }
        return winner;
    }

    // Get unregistered match results by coach
    public ArrayList<MatchDetailed> getUnregisteredPreviousMatchResultsByCoach(String coach) {
        ArrayList<MatchDetailed> unregisteredResults = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id  " +
                            "WHERE (home_team.coach = ? OR away_team.coach = ?) " +
                            "AND match.start_time < NOW() AND match.cancelled = false " +
                            "AND match.id NOT IN (SELECT match_id FROM match_result)");
            prep.setString(1, coach);
            prep.setString(2, coach);

            ResultSet result = prep.executeQuery();

            while(result.next()){unregisteredResults.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return unregisteredResults;
    }
}
