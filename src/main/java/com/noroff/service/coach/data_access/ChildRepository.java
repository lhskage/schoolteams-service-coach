package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Child;
import org.springframework.http.HttpStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ChildRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public ArrayList<Child> getParentsOfChild(String username) {
        ArrayList<Child> parents = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT parent_1, child, parent_2 FROM child_parent " +
                    "JOIN player " +
                    "ON child_parent.child = player.user_username " +
                    "JOIN users " +
                    "ON player.user_username = users.username " +
                    "WHERE player.user_username = ? AND users.role_id = 4 ");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                parents.add(
                    new Child(
                        result.getString("parent_1"),
                        result.getString("child"),
                        result.getString("parent_2")
                    )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return parents;
    }

    public HttpStatus addChildWithTwoParents(Child child){

        HttpStatus success;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO child_parent(parent_1, child, parent_2) " +
                            "VALUES (?, ?, ?)");

            prep.setString(1, child.getParent_1());
            prep.setString(2, child.getChild());
            prep.setString(3, child.getParent_2());

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    public HttpStatus addChildWithOneParent(Child child){

        HttpStatus success;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO child_parent(parent_1, child) " +
                            "VALUES (?, ?)");

            prep.setString(1, child.getParent_1());
            prep.setString(2, child.getChild());

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    public HttpStatus updateChildParentRelationship(String username, Child child) {
        HttpStatus success;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE child_parent " +
                            "SET parent_2 = ? " +
                            "WHERE child = ?;");

            prep.setString(1, child.getParent_2());
            prep.setString(2, username);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }
}
