package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Player;
import com.noroff.service.coach.models.User;
import org.springframework.http.HttpStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class PlayerRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public HttpStatus addPlayer(Player player) {
        HttpStatus success;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO player (user_username, team_id, player_number) " +
                            "VALUES (?, ?, ?)");

            prep.setString(1, player.getUsername());
            prep.setInt(2, player.getTeam_id());
            prep.setString(3, player.getPlayer_number());

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    public HttpStatus updatePlayerNumber(int team_id, String username, Player player) {
        HttpStatus success;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE PLAYER SET player_number = ?" +
                            "WHERE user_username = ? AND team_id = ?;");

            prep.setString(1, player.getPlayer_number());
            prep.setString(2, username);
            prep.setInt(3, team_id);

            int result = prep.executeUpdate();
            if (result != 0) {
                success = HttpStatus.CREATED;
            } else {
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }

        return success;
    }

    public ArrayList<Player> getAllPlayers() {
        ArrayList<Player> players = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM player ");
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                players.add(
                    new Player(
                            result.getString("user_username"),
                            result.getInt("team_id"),
                            result.getString("player_number")
                    )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return players;
    }

    public ArrayList<Player> getPlayersByTeam(int team_id) {
        ArrayList<Player> players = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM player WHERE team_id = ?");
            prep.setInt(1, team_id);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                players.add(
                        new Player(
                                result.getString("user_username"),
                                result.getInt("team_id"),
                                result.getString("player_number")
                        )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return players;
    }

    public ArrayList<User> getPlayersNotInTeam(int team_id) {
        ArrayList<User> users = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users " +
                    "WHERE users.username NOT IN (SELECT player.user_username FROM player WHERE player.team_id = ?) " +
                    "AND role_id = 4");
            prep.setInt(1, team_id);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                users.add(
                    new User(
                        result.getString("username"),
                        result.getInt("person_id"),
                        result.getInt("role_id")
                    )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return users;
    }

    public ArrayList<Player> getPlayersBySchool(int school_id) {
        ArrayList<Player> players = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM player " +
                    "JOIN team " +
                    "ON player.team_id = team.id " +
                    "WHERE team.school_id = ?");
            prep.setInt(1, school_id);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                players.add(
                        new Player(
                                result.getString("user_username"),
                                result.getInt("team_id"),
                                result.getString("player_number")
                        )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return players;
    }
}
