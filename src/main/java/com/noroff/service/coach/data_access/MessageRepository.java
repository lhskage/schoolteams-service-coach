package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Message;
import com.noroff.service.coach.models.Pair;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class MessageRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public ArrayList<Message> getSentMessages(String username) {
        ArrayList<Message> messages = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM message WHERE sender = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){messages.add(new Message(
                    result.getString("sender"),
                    result.getString("receiver"),
                    result.getString("message"),
                    result.getInt("message_id")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return messages;
    }

    public ArrayList<Message> getReceivedMessages(String username) {
        ArrayList<Message> messages = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM message WHERE receiver = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){messages.add(new Message(
                    result.getString("sender"),
                    result.getString("receiver"),
                    result.getString("message"),
                    result.getInt("message_id")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return messages;
    }

    public Pair<HttpStatus, Integer> sendMessage(Message message) {
        HttpStatus status;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO message(sender, receiver, message) " +
                            " VALUES (?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, message.getSender());
            prep.setString(2, message.getReceiver());
            prep.setString(3, message.getMessage());

            int result = prep.executeUpdate();
            if(result != 0){
                status = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt("message_id");
                    return new Pair<>(status, last_inserted_id);
                }
            }else{
                status = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(status, -1);
    }

    public HttpStatus sendNotification(String senderUsername, String receiverUsername, String message){
        HttpStatus success;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO message(sender, receiver, message)" +
                            " VALUES (?, ?, ?);");
            prep.setString(1, senderUsername);
            prep.setString(2, receiverUsername);
            prep.setString(3, message);


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    public HttpStatus notifyParentsBySchool(int school_id, Message message){
        HttpStatus status = HttpStatus.OK;
        ArrayList<String> parentUsername = new ArrayList<>();

        //Getting all usernames of the parents
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT DISTINCT p.parent_1, p.parent_2 " +
                    "FROM child_parent AS p " +
                    "JOIN player " +
                    "ON p.child = player.user_username " +
                    "JOIN team " +
                    "ON player.team_id = team.id " +
                    "JOIN school " +
                    "ON team.school_id = school.id " +
                    "WHERE school.id = ?;");
            prep.setInt(1, school_id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                parentUsername.add(result.getString("parent_1"));
                if(result.getString("parent_2") != null && !result.getString("parent_2").equals("null")){
                    parentUsername.add(result.getString("parent_2"));
                }
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        parentUsername = removeDuplicatesFromList(parentUsername);

        System.out.println(parentUsername.size());
        for(int i = 0; i < parentUsername.size(); i++) {
            System.out.println(parentUsername.get(i));
            if(parentUsername.get(i) != null || !parentUsername.get(i).equals("null")){
                sendNotification(message.getSender(), parentUsername.get(i), message.getMessage());
            }
        }
        return status;
    }

    public HttpStatus notifyParentsByTeam(int team_id, Message message){
        HttpStatus status = HttpStatus.OK;
        ArrayList<String> parentUsername = new ArrayList<>();

        //Getting all usernames of the parents
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT p.parent_1, p.parent_2 " +
                    "FROM child_parent AS p " +
                    "JOIN player " +
                    "ON p.child = player.user_username " +
                    "JOIN team " +
                    "ON player.team_id = team.id " +
                    "WHERE team.id = ?;");
            prep.setInt(1, team_id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                parentUsername.add(result.getString("parent_1"));
                if(result.getString("parent_2") != null && !result.getString("parent_2").equals("null")){
                    parentUsername.add(result.getString("parent_2"));
                }

            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }
        parentUsername = removeDuplicatesFromList(parentUsername);

        if(parentUsername.size() == 0) {
            status = HttpStatus.NOT_ACCEPTABLE;
        }
        for(int i = 0; i < parentUsername.size(); i++) {
            System.out.println(parentUsername.get(i));
            if(parentUsername.get(i) != null || !parentUsername.get(i).equals("null")){
                sendNotification(message.getSender(), parentUsername.get(i), message.getMessage());
            }
        }

        return status;
    }

    public HttpStatus notifyParentsBySport(int sport_id, Message message){
        HttpStatus status = HttpStatus.OK;
        ArrayList<String> parentUsername = new ArrayList<>();

        //Getting all usernames of the parents
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT p.parent_1, p.parent_2 " +
                    "FROM child_parent AS p " +
                    "JOIN player " +
                    "ON p.child = player.user_username " +
                    "JOIN team " +
                    "ON player.team_id = team.id " +
                    "JOIN sport " +
                    "ON team.sport_id = sport.id " +
                    "WHERE sport.id = ?;");
            prep.setInt(1, sport_id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                parentUsername.add(result.getString("parent_1"));
                if(result.getString("parent_2") != null && !result.getString("parent_2").equals("null")){
                    parentUsername.add(result.getString("parent_2"));
                }
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }
        parentUsername = removeDuplicatesFromList(parentUsername);

        if(parentUsername.size() == 0) {
            status = HttpStatus.NOT_ACCEPTABLE;
        }
        System.out.println(parentUsername.size());
        for(int i = 0; i < parentUsername.size(); i++) {
            System.out.println(parentUsername.get(i));
            if(parentUsername.get(i) != null || !parentUsername.get(i).equals("null")){
                sendNotification(message.getSender(), parentUsername.get(i), message.getMessage());
            }
        }

        return status;
    }

    public HttpStatus notifyParentsOfPlayer(String username, Message message){
        HttpStatus status = HttpStatus.OK;
        ArrayList<String> parentUsername = new ArrayList<>();

        //Getting all usernames of the parents
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT p.parent_1, p.parent_2 " +
                    "FROM child_parent AS p " +
                    "JOIN player " +
                    "ON p.child = player.user_username " +
                    "JOIN team " +
                    "ON player.team_id = team.id " +
                    "JOIN sport " +
                    "ON team.sport_id = sport.id " +
                    "where player.user_username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                parentUsername.add(result.getString("parent_1"));
                if(result.getString("parent_2") != null && !result.getString("parent_2").equals("null")){
                    parentUsername.add(result.getString("parent_2"));
                }
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }
        parentUsername = removeDuplicatesFromList(parentUsername);

        if(parentUsername.size() == 0) {
            status = HttpStatus.NOT_ACCEPTABLE;
        }
        System.out.println(parentUsername.size());
        for(int i = 0; i < parentUsername.size(); i++) {
            System.out.println(parentUsername.get(i));
            if(parentUsername.get(i) != null || !parentUsername.get(i).equals("null")){
                sendNotification(message.getSender(), parentUsername.get(i), message.getMessage());
            }
        }

        return status;
    }

    public ArrayList<String> removeDuplicatesFromList(ArrayList<String> arrayWithDuplicates) {
        ArrayList<String> arrayWithoutDuplicates = new ArrayList<>();
        for(String element : arrayWithDuplicates) {
            if(!arrayWithoutDuplicates.contains(element)) {
                arrayWithoutDuplicates.add(element);
            }
        }
        return arrayWithoutDuplicates;
    }
}