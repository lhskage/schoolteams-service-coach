package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Pair;
import com.noroff.service.coach.models.Sport;
import com.noroff.service.coach.models.Team;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class SportRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    public ArrayList<Sport> getAllSports() {
        ArrayList<Sport> sports = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM sport;");
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                sports.add(
                    new Sport(
                        result.getInt("id"),
                        result.getString("name")
                    )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return sports;
    }

    public Pair<HttpStatus, Integer> addSport(Sport sport) {
        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO sport (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);

            prep.setString(1, sport.getName());

            int result = prep.executeUpdate();
            if (result != 0) {
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if (rs.next()) {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(success, last_inserted_id);
                }
            } else {
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    public Pair<HttpStatus, Integer> updateSport(int id, Sport sport) {
        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE sport SET name = ? WHERE id = ?", Statement.RETURN_GENERATED_KEYS);

            prep.setString(1, sport.getName());
            prep.setInt(2, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next()) {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }
}