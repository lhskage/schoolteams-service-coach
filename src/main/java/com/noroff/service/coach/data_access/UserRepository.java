package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Toggle2FA;
import com.noroff.service.coach.models.User;
import org.springframework.http.HttpStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class UserRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public HttpStatus addUser(User user) {
        HttpStatus status;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO users(username, password, person_id, role_id)" +
                            " VALUES (?, ?, ?, 2);");
            prep.setString(1, user.getUsername());
            prep.setString(2, user.getPassword());
            prep.setInt(3, user.getPerson_id());

            int result = prep.executeUpdate();
            if(result != 0){
                status = HttpStatus.CREATED;
            }else{
                status = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return status;
    }

    public HttpStatus updateUser(String username, User user) {
        HttpStatus success;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE users " +
                            "SET (username, password) = (?, ?) " +
                            "WHERE username = ?");

            prep.setString(1, user.getUsername());
            prep.setString(2, user.getPassword());
            prep.setString(3, username);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    public ArrayList<User> getAllUsers() {
        ArrayList<User> users = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users");
            ResultSet result = prep.executeQuery();


            while(result.next()){ users.add(new User(
                    result.getString("username"),
                    result.getInt("person_id"),
                    result.getInt("role_id")
            ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return users;
    }

    public ArrayList<User> getAllAdministrators() {
        ArrayList<User> users = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users WHERE role_id = 1");
            ResultSet result = prep.executeQuery();


            while(result.next()){ users.add(new User(
                    result.getString("username"),
                    result.getInt("person_id"),
                    result.getInt("role_id")
            ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return users;
    }

    public ArrayList<User> getAllCoaches() {
        ArrayList<User> users = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users WHERE role_id = 2");
            ResultSet result = prep.executeQuery();


            while(result.next()){ users.add(new User(
                    result.getString("username"),
                    result.getInt("person_id"),
                    result.getInt("role_id")
            ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return users;
    }

    public ArrayList<User> getAllParents() {
        ArrayList<User> users = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users WHERE role_id = 3");
            ResultSet result = prep.executeQuery();


            while(result.next()){ users.add(new User(
                    result.getString("username"),
                    result.getInt("person_id"),
                    result.getInt("role_id")
            ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return users;
    }

    public ArrayList<User> getAllPlayers() {
        ArrayList<User> users = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users WHERE role_id = 4");
            ResultSet result = prep.executeQuery();


            while(result.next()){ users.add(new User(
                    result.getString("username"),
                    result.getInt("person_id"),
                    result.getInt("role_id")
            ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return users;
    }

    public Boolean getActive(String username){
        Boolean active = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT \"2fa_active\" FROM users WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                active = result.getBoolean("2fa_active");
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return active;
    }

    public Boolean update2FA(Toggle2FA toggle2FA){
        Boolean success = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE public.users SET \"2fa_active\"=?" +
                            " WHERE username = ?;");
            prep.setBoolean(1, toggle2FA.getActive());
            prep.setString(2, toggle2FA.getUsername());

            int result = prep.executeUpdate();
            if(result != 0){
                success = toggle2FA.getActive();
            }else{
                success = null;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return false;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    public String getQrUrl(String username){
        String url = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT tfa_qr_url FROM users WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                url = result.getString("tfa_qr_url");
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return url;
    }
}
