package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Role;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class RoleRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public Role getRole(int roleId) {
        Role role = null;

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM role WHERE id = ?");
            prep.setInt(1, roleId);
            ResultSet result = prep.executeQuery();

            while(result.next()){role = new Role(
                    result.getInt("id"),
                    result.getString("name"));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return role;
    }

    public Role getRoleByUsername(String username) {
        Role role = null;

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT id, name FROM role JOIN users " +
                    "ON role.id = users.role_id WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){role = new Role(
                    result.getInt("id"),
                    result.getString("name"));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return role;
    }

}
