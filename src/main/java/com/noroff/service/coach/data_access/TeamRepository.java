package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Pair;
import com.noroff.service.coach.models.Team;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class TeamRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public Pair<HttpStatus, Integer> addTeam(Team team) {
        HttpStatus success;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO team (name, school_id, coach, sport_id) VALUES " +
                            "(?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

            prep.setString(1, team.getName());
            prep.setInt(2, team.getSchool_id());
            prep.setString(3, team.getCoach());
            prep.setInt(4, team.getSport_id());

            int result = prep.executeUpdate();
            if (result != 0) {
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if (rs.next()) {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(success, last_inserted_id);
                }
            } else {
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    public Pair<HttpStatus, Integer> updateTeam(int id, Team team) {
        HttpStatus success;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE team " +
                            "SET name = ?, school_id = ?, coach = ?, sport_id = ? " +
                            "WHERE id = ?", Statement.RETURN_GENERATED_KEYS);

            prep.setString(1, team.getName());
            prep.setInt(2, team.getSchool_id());
            prep.setString(3, team.getCoach());
            prep.setInt(4, team.getSport_id());
            prep.setInt(5, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next()) {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    public ArrayList<Team> getAllTeams() {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team;");
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                        new Team(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getInt("school_id"),
                                result.getString("coach"),
                                result.getInt("sport_id")
                        )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }

    public Team getTeamById(int team_id) {
        Team team = null;

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team " +
                    "WHERE id = ?;");
            prep.setInt(1, team_id);
            ResultSet result = prep.executeQuery();

            while(result.next()){team = new Team(
                result.getInt("id"),
                result.getString("name"),
                result.getInt("school_id"),
                result.getString("coach"),
                result.getInt("sport_id")
            );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return team;
    }

    public ArrayList<Team> getTeamsBySchool(int school_id) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team " +
                    "WHERE school_id = ?;");
            prep.setInt(1, school_id);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                    new Team(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("school_id"),
                        result.getString("coach"),
                        result.getInt("sport_id")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }

    public ArrayList<Team> getTeamByCoach(String coach) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team " +
                    "WHERE coach = ?;");
            prep.setString(1, coach);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                        new Team(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getInt("school_id"),
                                result.getString("coach"),
                                result.getInt("sport_id")
                        ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }

    public ArrayList<Team> getTeamBySport(int sport_id) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team " +
                    "WHERE sport_id = ?;");
            prep.setInt(1, sport_id);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                    new Team(
                            result.getInt("id"),
                            result.getString("name"),
                            result.getInt("school_id"),
                            result.getString("coach"),
                            result.getInt("sport_id")
                    )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }

    public ArrayList<Team> getTeamByPlayer(String player) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT team.id, team.name, team.school_id, team.coach, team.sport_id " +
                    "FROM team " +
                    "JOIN player " +
                    "ON team.id = player.team_id " +
                    "WHERE player.user_username = ?;");
            prep.setString(1, player);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                        new Team(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getInt("school_id"),
                                result.getString("coach"),
                                result.getInt("sport_id")
                        ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }
}
