package com.noroff.service.coach.data_access;



import com.noroff.service.coach.models.Suggestion;
import org.springframework.http.HttpStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SuggestionRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public ArrayList<Suggestion> getSuggestionsByRequester(String username) {
        ArrayList<Suggestion> suggestions = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM school_change_suggestion WHERE requester = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){suggestions.add(new Suggestion(
                    result.getInt("school_id"),
                    result.getString("name"),
                    result.getString("comment"),
                    result.getBoolean("approved"),
                    result.getString("requester")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return suggestions;
    }

    public HttpStatus createSuggestion(Suggestion suggestion) {
        HttpStatus status;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO school_change_suggestion(school_id, name, comment, requester) VALUES " +
                                    "(?, ?, ?, ?)");
            prep.setInt(1, suggestion.getId());
            prep.setString(2, suggestion.getName());
            prep.setString(3, suggestion.getComment());
            prep.setString(4, suggestion.getRequester());

            int result = prep.executeUpdate();
            if(result != 0){
                status = HttpStatus.CREATED;
            }else{
                status = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return status;
    }
}
