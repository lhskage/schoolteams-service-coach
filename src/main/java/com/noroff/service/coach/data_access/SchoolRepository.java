package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Pair;
import com.noroff.service.coach.models.School;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class SchoolRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    public ArrayList<School> getAllSchools() {
        ArrayList<School> schools = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM school;");
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                schools.add(
                    new School(
                        result.getInt("id"),
                        result.getString("name")
                    )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return schools;
    }

    public Pair<HttpStatus, Integer> addSchool(School school) {
        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO school(name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);

            prep.setString(1, school.getName());

            int result = prep.executeUpdate();
            if (result != 0) {
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if (rs.next()) {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(success, last_inserted_id);
                }
            } else {
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    public Pair<HttpStatus, Integer> updateSchool(int id, School school) {
        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE school\n" +
                            "SET name = ? " +
                            "WHERE id = ?; ", Statement.RETURN_GENERATED_KEYS);

            prep.setString(1, school.getName());
            prep.setInt(2, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next()) {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    public School getSchoolByPlayer(String username) {
        School school = new School();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement(
                    "SELECT school.id, school.name FROM school " +
                            "JOIN team " +
                            "ON school.id = team.school_id " +
                            "JOIN player " +
                            "ON team.id = player.team_id " +
                            "WHERE player.user_username = ?");
            prep.setString(1, username);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                school = new School(
                    result.getInt("id"),
                    result.getString("name")
                );
            }
        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return school;
    }

    public School getSchoolByUsername(String username) {
        School school = new School();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement(
                    "SELECT DISTINCT school.id, school.name FROM school " +
                            "JOIN team " +
                            "ON school.id = team.school_id " +
                            "JOIN player " +
                            "ON team.id = player.team_id " +
                            "JOIN users " +
                            "ON player.user_username = users.username " +
                            "WHERE users.username = ? OR team.coach = ?");
            prep.setString(1, username);
            prep.setString(2, username);


            ResultSet result = prep.executeQuery();
            while(result.next()) {
                school = new School(
                        result.getInt("id"),
                        result.getString("name")
                );
            }
        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return school;
    }
}