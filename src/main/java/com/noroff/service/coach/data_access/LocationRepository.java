package com.noroff.service.coach.data_access;

import com.noroff.service.coach.models.Location;
import com.noroff.service.coach.models.Pair;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class LocationRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public ArrayList<Location> getAllLocations() {
        ArrayList<Location> locations = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM location;");
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                locations.add(
                    new Location(
                            result.getInt("id"),
                            result.getString("street"),
                            result.getString("number"),
                            result.getString("postal_code"),
                            result.getString("name")
                    )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return locations;
    }

    public Pair<HttpStatus, Integer> addLocation(Location location) {
        HttpStatus success;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO location(street, number, postal_code, name)\n" +
                            "VALUES (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setObject(1, location.getStreet());
            prep.setString(2, location.getNumber());
            prep.setString(3, location.getPostal_code());
            prep.setString(4, location.getName());

            int result = prep.executeUpdate();
            if (result != 0) {
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if (rs.next()) {
                    last_inserted_id = rs.getInt("id");
                    return new Pair<>(success, last_inserted_id);
                }
            } else {
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    public Pair<HttpStatus, Integer> updateLocation(int id, Location location) {
        HttpStatus success;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE location SET (street, number, name, postal_code) " +
                            "= (?, ?, ?, ?) WHERE id = ?;", Statement.RETURN_GENERATED_KEYS);

            prep.setString(1, location.getStreet());
            prep.setString(2, location.getNumber());
            prep.setString(3, location.getName());
            prep.setString(4, location.getPostal_code());
            prep.setInt(5, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next()) {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }
}
